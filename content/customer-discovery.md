+++
date = "2018-05-30"
title = "PhD to CEO - Customer Discovery Resources"
layout = "single"
name = "Test"
tagline = "Test."
+++

# *PhD to CEO - Customer Discovery Resources*




---




- [Steve Blank - The Startup Owner's Manual](https://www.amazon.com/Startup-Owners-Manual-Step-Step/dp/0984999302/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1532315620&sr=1-1&keywords=steve+blank&linkCode=ll1&tag=rawwerks09-20&linkId=c56b37b1019c5299f544a89d1625c4cb)
- [Steve Blank - The Four Steps to the Epiphany](https://www.amazon.com/Four-Steps-Epiphany-Steve-Blank/dp/0989200507/ref=as_li_ss_tl?_encoding=UTF8&pd_rd_i=0989200507&pd_rd_r=69978871-8e26-11e8-9f96-9391f60fc1b0&pd_rd_w=GkX1v&pd_rd_wg=UjJOb&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=2610440344683357453&pf_rd_r=KJQ3PR9WHPGTY0RXZE4S&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=KJQ3PR9WHPGTY0RXZE4S&linkCode=ll1&tag=rawwerks09-20&linkId=652c43f14e519a9c668011cb2918a8cd)
- [Steve Blank - Free Online Course 'How to Build a Startup'](https://www.udacity.com/course/how-to-build-a-startup--ep245)
- [Bill Aulet - Disciplined Entrepreneurship](https://www.amazon.com/Disciplined-Entrepreneurship-Steps-Successful-Startup/dp/1118692284/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1532315743&sr=1-1&keywords=aulet+disciplined+entrepreneurship&linkCode=ll1&tag=rawwerks09-20&linkId=8a0364b0549c8b083d1fab47401f0d3b)
- [Eric Ries - The Lean Startup](https://www.amazon.com/Lean-Startup-Entrepreneurs-Continuous-Innovation/dp/0307887898/ref=as_li_ss_tl?ie=UTF8&qid=1532315525&sr=8-1&keywords=eric+reis+lean+startup&linkCode=ll1&tag=rawwerks09-20&linkId=1380f5e43a11f031c43451262ffcec56)
- [Rob Fitzpatrick - The Mom Test](https://www.amazon.com/Mom-Test-customers-business-everyone/dp/1492180742/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=rawwerks09-20&linkId=4448cb2f05f7f2e9c42fd6a2803e0d79&language=en_US)
- [Giff Constable - Talking to Humans](https://www.amazon.com/Talking-Humans-Success-understanding-customers/dp/099080092X/ref=as_li_ss_tl?ie=UTF8&qid=1533176842&sr=8-1&keywords=talking+to+humans&linkCode=ll1&tag=rawwerks09-20&linkId=17c46cecfadf51b30b9c1c46e94c2cc2&language=en_US)
- [Lab Rat Launchpad](https://labratlaunchpad.com/p/labrat-launchpad)



