+++
date = "2020-12-31"
title = "PhD to CEO - Deep Work Books"
layout = "single"

+++

# *PhD to CEO - Deep Work Books*




---


- [Cal Newport - Deep Work](https://www.amazon.com/Deep-Work-Focused-Success-Distracted/dp/1455586692/ref=as_li_ss_tl?keywords=deep+work&pd_rd_i=1455586692&pd_rd_r=9dd850ff-ec80-493c-9a1f-254f46127fe2&pd_rd_w=fiqbs&pd_rd_wg=gLAbu&pf_rd_p=b0a90583-d22c-4c32-806b-f09cd6946e61&pf_rd_r=1NF62N9RZP2BGS5C52KR&qid=1560360415&s=gateway&linkCode=ll1&tag=rawwerks09-20&linkId=f6e0b9e3a22a53cc65a37a4a81f43dc7&language=en_US)
- [Cal Newport - Digital Minimalism](https://www.amazon.com/Digital-Minimalism-Choosing-Focused-Noisy/dp/0525536515/ref=as_li_ss_tl?_encoding=UTF8&pd_rd_i=0525536515&pd_rd_r=49409e04-8d37-11e9-a0e4-e193d282c7b6&pd_rd_w=90Zbm&pd_rd_wg=OM6GM&pf_rd_p=a2006322-0bc0-4db9-a08e-d168c18ce6f0&pf_rd_r=3576XAJMW1A7YA5R72HV&psc=1&refRID=3576XAJMW1A7YA5R72HV&linkCode=ll1&tag=rawwerks09-20&linkId=5210ef9d1ab5f090d5156849b7703e10&language=en_US)
- [Scott Youg - Ultralearning](https://www.amazon.com/Ultralearning-Scott-Young-audiobook/dp/B07ST3Z1Q6/ref=as_li_ss_tl?dchild=1&keywords=ultralearning&qid=1609454588&sr=8-1&returnFromLogin=1&linkCode=ll1&tag=rawwerks09-20&linkId=4e2d49597bba04399dbd93c646f6f4b9&language=en_US)
- [Richard Koch - The 80/20 Principle: The Secret to Achieving More with Less](https://www.amazon.com/80-20-Principle-Secret-Achieving/dp/0385491743/ref=as_li_ss_tl?crid=KA0FEPATBASX&keywords=8020+principle&qid=1557938921&s=gateway&sprefix=8020+princi,aps,218&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=9512acfcf53f0a5c99ccfe9f338741b8&language=en_US)
- [Greg McKeown - Essentialism: The Disciplined Pursuit of Less](https://www.amazon.com/Essentialism-Disciplined-Pursuit-Greg-McKeown/dp/0804137382/ref=as_li_ss_tl?crid=3H9JP2J0E9I1Q&keywords=essentialism+by+greg+mckeown&qid=1557939038&s=gateway&sprefix=essentialism,aps,198&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=c8065d8854e3833133d8371156674d9e&language=en_US)
- [Jake Knapp and John Zeratsky - Make Time: How to Focus on What Matters Every Day](https://www.amazon.com/Make-Time-Focus-Matters-Every/dp/0525572422/ref=as_li_ss_tl?crid=1MSKKEYJVH2DG&keywords=make+time+how+to+focus+on+what+matters+everyday&qid=1557939181&s=gateway&sprefix=make+time,aps,206&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=219f933d2c84713ae4ec7f63c98bf254&language=en_US)
- [Peter Drucker - The Effective Executive: The Definitive Guide to Getting the Right Things Done](https://www.amazon.com/Effective-Executive-Definitive-Harperbusiness-Essentials/dp/0060833459/ref=as_li_ss_tl?crid=1TONW1PRSNNWU&keywords=effective+executive+drucker&qid=1557939229&s=books&sprefix=effective+exec,aps,190&sr=1-1&linkCode=ll1&tag=rawwerks09-20&linkId=cdc866c1e2236eb208166a4580d19512&language=en_US)





