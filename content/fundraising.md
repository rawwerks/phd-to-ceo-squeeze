+++
date = "2018-08-04"
title = "PhD to CEO - Fundraising Resources"
layout = "single"
name = "Raymond Weitekamp"
tagline = "Fundraising Resources"
+++

# *PhD to CEO - Fundraising Resources*




---




- [SAFE Notes: Everything You Need to Know](https://www.upcounsel.com/safe-notes)
- [Brad Feld & Jason Mendelson - Venture Deals](https://www.amazon.com/Venture-Deals-Smarter-Lawyer-Capitalist/dp/1119259754/ref=as_li_ss_tl?ie=UTF8&qid=1533410238&sr=8-1&keywords=brad+feld+venture+deals&linkCode=ll1&tag=rawwerks09-20&linkId=1be2179e163f76446e62f2d9209a98a3&language=en_US)
- [Y Combinator - Startup Documents](http://www.ycombinator.com/documents/)
- [Lab Rat Launchpad](https://labratlaunchpad.com/p/labrat-launchpad)



