+++
date = "2018-05-30"
title = "PhD to CEO - Interpersonal Communication Resources"
layout = "single"
name = "Test"
tagline = "Test."
+++

# *PhD to CEO - Interpersonal Communication Resources*




---




- [Marshall Rosenberg - Nonviolent Communication](https://www.amazon.com/Nonviolent-Communication-Language-Life-Changing-Relationships/dp/189200528X/ref=as_li_ss_tl?_encoding=UTF8&qid=1532915892&sr=1-3&linkCode=ll1&tag=rawwerks09-20&linkId=e135581ac8d33a4ec860ef1f4c59ad68&language=en_US)
- [Kim Scott - Radical Candor](https://www.amazon.com/Radical-Candor-Kick-Ass-Without-Humanity/dp/1250103509/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1532915829&sr=1-1&keywords=radical+candor&linkCode=ll1&tag=rawwerks09-20&linkId=65cb8b157236ffa8fda61dc13583861b&language=en_US)
- [Fort Light](https://www.fortlight.com/)
- [Wisdomlabs](https://wisdomlabs.com/)
- [Lab Rat Launchpad](https://labratlaunchpad.com/p/labrat-launchpad)


