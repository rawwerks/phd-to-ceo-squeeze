
+++
date = "2018-05-30"
title = "PhD to CEO - Open Kimono Inspiration"
layout = "single"
name = "Open Kimono"
tagline = "Open Kimono"

+++

# *PhD to CEO - Open Kimono Inspiration*


---


- [Rework - Jason Fried & David Heinemeier Hansson](https://www.amazon.com/Rework-Jason-Fried/dp/0307463745/ref=as_li_ss_tl?ie=UTF8&qid=1534113395&sr=8-1&keywords=rework&linkCode=ll1&tag=rawwerks09-20&linkId=6f8b65a8157fdee3c280197475f8acb2&language=en_US)
- [Tools of Titans - Tim Ferriss](https://www.amazon.com/Tools-Titans-Billionaires-World-Class-Performers/dp/1328683788/ref=as_li_ss_tl?ie=UTF8&qid=1534113340&sr=8-2&keywords=tools+of+titans&linkCode=ll1&tag=rawwerks09-20&linkId=001969eddb7352e4544fe2637aebc81c&language=en_US)
- [How to Stop Sucking and Be Awesome Instead - Jeff Atwood](https://www.amazon.com/How-Stop-Sucking-Awesome-Instead-ebook/dp/B00BU3KPQU/ref=as_li_ss_tl?ie=UTF8&qid=1534113477&sr=8-1&keywords=jeff+atwood&linkCode=ll1&tag=rawwerks09-20&linkId=4d8348653c6c8e5c80796da20e3c6294&language=en_US)
- [How to Fail at Almost Everything and Still Win Big - Scott Adams](https://www.amazon.com/How-Fail-Almost-Everything-Still/dp/1591847745/ref=as_li_ss_tl?ie=UTF8&qid=1534113656&sr=8-1&keywords=scott+adams+how+to+fail+at+almost+everything+and+still+win+big&linkCode=ll1&tag=rawwerks09-20&linkId=ca7d99eb3c5710066639bbde48acc755&language=en_US)
- [Principles - Ray Dalio](https://www.amazon.com/dp/1501124021/ref=as_li_ss_tl?psc=1&linkCode=ll1&tag=rawwerks09-20&linkId=ccb0bd141825f4dadf5510b3bf46e24a&language=en_US)
- [Incerto - Nassim Nicholas Taleb](https://www.amazon.com/Incerto-Fooled-Randomness-Procrustes-Antifragile/dp/0399590455/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1534113785&sr=1-1&keywords=incerto&linkCode=ll1&tag=rawwerks09-20&linkId=3dd089f3c543d66d8547f7d8d625a144&language=en_US)
- [Why Greatness Cannot Be Planned: The Myth of the Objective - Ken Stanley & Joel Lehman](https://www.amazon.com/Why-Greatness-Cannot-Planned-Objective/dp/3319155237/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1534113904&sr=1-1&keywords=ken+stanley&linkCode=ll1&tag=rawwerks09-20&linkId=c63d718268b57dca65d9db8492bd220d&language=en_US)
- [The Management Myth: Debunking Modern Business Philosophy - Matthew Stewart](https://www.amazon.com/Management-Myth-Debunking-Business-Philosophy/dp/0393338525/ref=as_li_ss_tl?ie=UTF8&qid=1534113539&sr=8-1&keywords=management+myth&linkCode=ll1&tag=rawwerks09-20&linkId=cc97f08a6dafde16b351e833c9be693f&language=en_US)
- [Launch - Jeff Walker](https://www.amazon.com/Launch-Internet-Millionaires-Anything-Business/dp/1630470171/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1534118148&sr=1-1&keywords=product+launch+formula&linkCode=ll1&tag=rawwerks09-20&linkId=106bee3369c0fee3424256f2d658f571&language=en_US)


