+++
date = "2018-05-30"
title = "PhD to CEO - Resources for Setting Priorities"
layout = "single"
name = "PhD to CEO - Resources for Setting Priorities"
tagline = "PhD to CEO - Resources for Setting Priorities"
+++

# *PhD to CEO - Resources for Setting Priorities*




---

- [Richard Koch - The 80/20 Principle: The Secret to Achieving More with Less](https://www.amazon.com/80-20-Principle-Secret-Achieving/dp/0385491743/ref=as_li_ss_tl?crid=KA0FEPATBASX&keywords=8020+principle&qid=1557938921&s=gateway&sprefix=8020+princi,aps,218&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=9512acfcf53f0a5c99ccfe9f338741b8&language=en_US)
- [Tim Ferriss - The 4-Hour Workweek](https://www.amazon.com/4-Hour-Workweek-Escape-Live-Anywhere/dp/0307465357/ref=as_li_ss_tl?crid=8VHYAXDZEM9L&keywords=4+hour+work+week&qid=1557938995&s=gateway&sprefix=4+hour+work,aps,225&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=4aac38b9e4ed61271ed2ce5639bf742f&language=en_US)
- [Greg McKeown - Essentialism: The Disciplined Pursuit of Less](https://www.amazon.com/Essentialism-Disciplined-Pursuit-Greg-McKeown/dp/0804137382/ref=as_li_ss_tl?crid=3H9JP2J0E9I1Q&keywords=essentialism+by+greg+mckeown&qid=1557939038&s=gateway&sprefix=essentialism,aps,198&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=c8065d8854e3833133d8371156674d9e&language=en_US)
- [William B. Irvine - A Guide to the Good Life: The Ancient Art of Stoic Joy](https://www.amazon.com/Guide-Good-Life-Ancient-Stoic-ebook/dp/B0040JHNQG/ref=as_li_ss_tl?keywords=stoic+joy&pd_rd_i=B0040JHNQG&pd_rd_r=a4bcb79e-d411-42be-ae47-6451558a4698&pd_rd_w=l7hbP&pd_rd_wg=9OVjc&pf_rd_p=f0479f98-a32d-45cd-9c12-7aaced42b1ec&pf_rd_r=4AKGAXXDQ05ZXY3X3KSD&qid=1557939090&s=gateway&linkCode=ll1&tag=rawwerks09-20&linkId=f0ae8406b04c9935e61f41d727e0f5a5&language=en_US)
- [Gary Keller - The ONE Thing: The Surprisingly Simple Truth Behind Extraordinary Results](https://www.amazon.com/ONE-Thing-Surprisingly-Extraordinary-Results/dp/1885167776/ref=as_li_ss_tl?keywords=one+thing&qid=1557939145&s=gateway&sr=8-1&linkCode=ll1&tag=rawwerks09-20&linkId=cfc7700dd3366e0b4963a1d57bcd8fe9&language=en_US)
- [Jake Knapp and John Zeratsky - Make Time: How to Focus on What Matters Every Day](https://www.amazon.com/Make-Time-Focus-Matters-Every/dp/0525572422/ref=as_li_ss_tl?crid=1MSKKEYJVH2DG&keywords=make+time+how+to+focus+on+what+matters+everyday&qid=1557939181&s=gateway&sprefix=make+time,aps,206&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=219f933d2c84713ae4ec7f63c98bf254&language=en_US)
- [Peter Drucker - The Effective Executive: The Definitive Guide to Getting the Right Things Done](https://www.amazon.com/Effective-Executive-Definitive-Harperbusiness-Essentials/dp/0060833459/ref=as_li_ss_tl?crid=1TONW1PRSNNWU&keywords=effective+executive+drucker&qid=1557939229&s=books&sprefix=effective+exec,aps,190&sr=1-1&linkCode=ll1&tag=rawwerks09-20&linkId=cdc866c1e2236eb208166a4580d19512&language=en_US)
- [Derek Sivers - Anything You Want: 40 Lessons for a New Kind of Entrepreneur](https://www.amazon.com/Anything-You-Want-Lessons-Entrepreneur/dp/1591848261/ref=as_li_ss_tl?crid=2MWQEFN42W46F&keywords=derek+sivers+anything+you+want&qid=1557939264&s=books&sprefix=derek+sivers,stripbooks,185&sr=1-3-catcorr&linkCode=ll1&tag=rawwerks09-20&linkId=fea7588e98afa4949d6cb3598923caeb&language=en_US)


