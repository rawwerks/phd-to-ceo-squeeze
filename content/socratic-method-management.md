+++
date = "2019-05-22"
title = "PhD to CEO - The Socratic Method for Management"
layout = "single"
name = "Raymond Weitekamp"
tagline = "The Socratic Method for Management."
+++

# *PhD to CEO - Books Related to the Socratic Method for Management*




---


- [The Coaching Habit: Say Less, Ask More & Change the Way You Lead Forever -  Michael Bungay Stanier](https://www.amazon.com/Coaching-Habit-Less-Change-Forever/dp/0978440749/ref=as_li_ss_tl?keywords=coaching+habit&pd_rd_i=0978440749&pd_rd_r=9996565b-1b6d-46b6-951b-cda3c936f650&pd_rd_w=0SWPi&pd_rd_wg=THR4Q&pf_rd_p=f0479f98-a32d-45cd-9c12-7aaced42b1ec&pf_rd_r=BDHQRNHMNS5RHF3WWYZ2&qid=1558539874&s=gateway&linkCode=ll1&tag=rawwerks09-20&linkId=3f268f773b04ad991cd67ce2de09f90b&language=en_US)
- [The Advantage - Patrick Lencioni](https://www.amazon.com/Advantage-Enhanced-Organizational-Everything-Business-ebook/dp/B006ORWT3Y/ref=as_li_ss_tl?ie=UTF8&qid=1535566189&sr=8-1&keywords=the+advantage+by+patrick+lencioni&linkCode=ll1&tag=rawwerks09-20&linkId=6f73ac8f90d5e990732465e5fc2beaf3&language=en_US)
- [Dare to Lead: Brave Work. Tough Conversations. Whole Hearts. - Brené Brown](https://www.amazon.com/Dare-Lead-Brave-Conversations-Hearts/dp/0399592520/ref=as_li_ss_tl?crid=1DYVIAIHEE4Q8&keywords=dare+to+lead+brene+brown&qid=1558539949&s=gateway&sprefix=dare+to+lead,aps,319&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=2e36e389cce49839dc7978770f1c7683&language=en_US)
- [Let Go: How to Transform Moments of Panic into a Life of Profits and Purpose - Pat Flynn](https://www.amazon.com/Let-Go-Transform-Moments-Profits/dp/0997082348/ref=as_li_ss_tl?keywords=pat+flynn+let+go&qid=1558540017&s=gateway&sr=8-1-fkmrnull&linkCode=ll1&tag=rawwerks09-20&linkId=8637194d0b6077a9a181f30f68462bba&language=en_US)

