+++
date = "2018-08-04"
title = "Thank you for sharing your feedback with PhD to CEO"
layout = "feedback"
name = "Raymond Weitekamp"
tagline = "Thank you for sharing your feedback with PhD to CEO"
+++




Thanks for your feedback! I really appreciate it. 

In case you're curious, here is [list of all of my tools pages](/tools-index).