+++
date = "2019-08-12"
title = "PhD to CEO - Top Travel Tools"
layout = "single"
name = "PhD to CEO - Top Travel Tools"
tagline = "PhD to CEO - Top Travel Tools"
+++

# *PhD to CEO - Top Travel Tools*




---




- [TheraZinc Oral Spray](https://www.amazon.com/Quantum-TheraZinc-Gluconate-Support-Soothing/dp/B003EIPCDK/ref=as_li_ss_tl?keywords=zinc+spray&qid=1565668092&s=gateway&sr=8-5&linkCode=ll1&tag=rawwerks09-20&linkId=f26675dabf970d26da9548e718bffc80&language=en_US) 5-8 Sprays every hour of your flight. Combine with Vitamin C if your stomach can handle. 
- [Rubz Foot Massager](https://www.amazon.com/Due-North-Massage-Plantar-Fasciitus/dp/B002QEY6NK/ref=as_li_ss_tl?crid=2DAX3LGMJ022D&keywords=rubz+foot+massage+ball&qid=1565668322&s=gateway&sprefix=rubz,aps,330&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=d75c4c4bbe8d0a3a5ecb79c114bf6d9b&language=en_US) Roll out your feet as soon as you get to your hotel/home.
- [Compression Pants](https://www.amazon.com/Compression-Pants-Tight-Layer-Leggings/dp/B00OKUP092/ref=as_li_ss_tl?keywords=compression+pants+circulation&qid=1565668527&s=gateway&sr=8-5&linkCode=ll1&tag=rawwerks09-20&linkId=c070b490b22a75722739835845be1f9b&language=en_US) I wear compression pants on every flight to keep circulation going.
- [Yoga Tune-Up Balls](https://www.amazon.com/Yoga-Tune-Millers-Therapy-Balls/dp/B00ERF6X4G/ref=as_li_ss_tl?crid=1L16RALHDSFSZ&keywords=yoga+tune+up+balls&qid=1565668610&s=gateway&sprefix=yoga+tune+,aps,288&sr=8-3&th=1&linkCode=ll1&tag=rawwerks09-20&linkId=df5b149416a1172e05d01b26462dbd3d&language=en_US) I put these behind my back as lumbar support during flight, use to self-massage after flight.
- [Magnetic Screen Protector](https://www.amazon.com/Kensington-MacBook-Magnetic-Privacy-K64490WW/dp/B077TVSTMT/ref=as_li_ss_tl?keywords=Kensington+Magnetic+Screen+Protector&qid=1565668686&s=gateway&sr=8-1&linkCode=ll1&tag=rawwerks09-20&linkId=2cce9b90c428e0510412ec8d09385f14&language=en_US) Don't let your neighbors spy on your work secrets!
- [Earasers - Musicians Earplugs](https://www.amazon.com/Earasers-Hi-Fi-Fidelity-Earplugs-Medium/dp/B01L4LKQK2/ref=as_li_ss_tl?keywords=earasers&qid=1565668740&s=gateway&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=b6d2234dbf667a4ddf7f69fa96de7196&language=en_US) These are volume reducing. I use them in loud spaces like trains, planes, airports - but when I still need to hear what's going on around me.
- [Sleepmaster Eye Mask](https://www.amazon.com/Sleep-Master-smblu01-Mask/dp/B0015NZ6FK/ref=as_li_ss_tl?crid=3BEKMVKWTEAED&keywords=sleepmaster+sleeping+mask&qid=1565668853&s=gateway&sprefix=sleepmaster,aps,297&sr=8-3&linkCode=ll1&tag=rawwerks09-20&linkId=a91b754c39e3eae9b171b9b3cf8955ac&language=en_US) Crucial for controlling your sleep schedule!
- [Athletic Greens Travel Packs](https://www.amazon.com/Athletic-Greens-Supplement-Multivitamin-Antioxidant/dp/B01IJ4OS9M/ref=as_li_ss_tl?ie=UTF8&linkCode=ll1&tag=rawwerks09-20&linkId=9af650553de61a186742b20918d81cfb&language=en_US) This is a great way to get 'greens' when you are on the road and have little control over your diet. 



