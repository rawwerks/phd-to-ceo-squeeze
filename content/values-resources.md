+++
date = "2018-08-04"
title = "PhD to CEO - Help to Find Your Founding Values"
layout = "single"
name = "Raymond Weitekamp"
tagline = "Help to Find Your Founding Values"
+++

# *PhD to CEO - Help to Find Your Founding Values*




---



- [The Advantage - Patrick Lencioni](https://www.amazon.com/Advantage-Enhanced-Organizational-Everything-Business-ebook/dp/B006ORWT3Y/ref=as_li_ss_tl?ie=UTF8&qid=1535566189&sr=8-1&keywords=the+advantage+by+patrick+lencioni&linkCode=ll1&tag=rawwerks09-20&linkId=6f73ac8f90d5e990732465e5fc2beaf3&language=en_US)
- [The E-Myth Revisited - Michael Gerber](https://www.amazon.com/Myth-Revisited-Small-Businesses-About-ebook/dp/B000RO9VJK/ref=as_li_ss_tl?ie=UTF8&qid=1535566104&sr=8-1&keywords=e-myth+revisited+by+michael+gerber&linkCode=ll1&tag=rawwerks09-20&linkId=97587b14d426fe2ccaa342883b402ff2&language=en_US)
- [Will it Fly? - Pat Flynn](https://www.amazon.com/Will-Fly-Business-Waste-Money-ebook/dp/B01BCLPPAK/ref=as_li_ss_tl?ie=UTF8&qid=1535566226&sr=8-1&keywords=will+it+fly+pat+flynn&linkCode=ll1&tag=rawwerks09-20&linkId=56bb85d3f55ff6744f2fb07283dd6959&language=en_US)
- [The Founder's Dilemmas - Noam Wasserman](https://www.amazon.com/Founders-Dilemmas-Anticipating-Foundation-Entrepreneurship-ebook/dp/B007AIXKUM/ref=as_li_ss_tl?ie=UTF8&qid=1535566265&sr=8-1&keywords=founder's+dilemma&linkCode=ll1&tag=rawwerks09-20&linkId=1ca88ab23c0588e0364cd7807ca6267b&language=en_US)
- [Mark Nicolson](http://nicolsongroup.com/)
- [Lab Rat Launchpad](https://labratlaunchpad.com/p/labrat-launchpad)

